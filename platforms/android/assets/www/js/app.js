(function() {
    var app;

    app = angular.module('bibik', ["ngRoute"]);

    app.config([
        "$routeProvider", function($routeProvider) {
          $routeProvider.when("/", {
            templateUrl: "template/home.html",
            controller: "indexCtrl"
          }).when("/details/:id", {
            templateUrl: "template/detail.html",
            controller: "detailsCtrl"
          }).otherwise({
            redirectTo: "/"
          });
        }
    ]);

    app.controller("indexCtrl", ["$scope","$http", function($scope, $http) {
        $scope.products = [];
        $scope.term = '';
        $scope.hasTerm = false;
        
        $scope.search = function() {
            if($scope.term.length > 0) {
                $http.get("http://ca.bibik.cz/api/1/products/search?term="+ encodeURIComponent($scope.term)).success(function(data) {
                    $scope.products = data;
                    $scope.hasTerm = true;
                });
            } else {
                setProductData();
            }
        };
        function setProductData() {
            $http.get("http://ca.bibik.cz/api/1/products").success(function(data) {
                $scope.products = data;
                $scope.hasTerm = false;
            });
        }
        setProductData();

        return $scope;
    }]);

    app.controller("detailsCtrl", ["$scope", "$http", "$routeParams", function($scope, $http, $routeParams) {
        return $http.get("http://ca.bibik.cz/api/1/products/detail?id=" + $routeParams.id).success(function(data) {
            $scope.data = data;
        });
    }]);

})();
