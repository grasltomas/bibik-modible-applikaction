(function() {
    var app;

    app = angular.module('bibik', ["ngRoute", "ngAnimate"]);

    app.config([
        "$routeProvider", function($routeProvider) {
          $routeProvider.when("/", {
            templateUrl: "template/home.html",
            controller: "indexCtrl"
          }).when("/details/:id", {
            templateUrl: "template/detail.html",
            controller: "detailsCtrl",
            animate: "slide-left"
          }).otherwise({
            redirectTo: "/"
          });
        }
    ]);

    app.controller("indexCtrl", ["$scope","$http", function($scope, $http) {
        $scope.products = [];
        $scope.term = '';
        $scope.hasTerm = false;
        $scope.loading = false;
        
        $scope.$on('$routeChangeStart', function(next, current) { 
            console.log("test");
            
            
         });        
        
        $scope.search = function() {
            if($scope.term.length > 0) {
                $http.get("http://ca.bibik.cz/api/1/products/search?term="+ encodeURIComponent($scope.term)).success(function(data) {
                    $scope.products = data;
                    $scope.hasTerm = true;
                });
            } else {
                setProductData();
            }
        };
        function setProductData() {
            $http.get("http://ca.bibik.cz/api/1/products").success(function(data) {
                $scope.products = data;
                $scope.hasTerm = false;
            });
        }
        setProductData();

        return $scope;
    }]);

    app.controller("detailsCtrl", ["$scope", "$http", "$routeParams", function($scope, $http, $routeParams) {
                        $scope.loading = true;

        return $http.get("http://ca.bibik.cz/api/1/products/detail?id=" + $routeParams.id).success(function(data) {
            $scope.data = data;
                                    $scope.loading = false;

        });
    }]);

    app.directive('animClass',["$route", function($route) {
        return {
                link: function(scope, element, attrs){

                var enterClass = $route.current.animate;
                
                scope.$on('$destroy',function(){
                    element.css("top", scope.position);
                    element.removeClass(enterClass);
                    element.addClass($route.current.animate);
                });
            }
        };
    }]);

    app.directive('scrolly', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var raw = element[0];
                console.log('loading directive');

                element.bind('scroll', function () {
                    scope.position = raw.scrollTop + 50;
                });
            }
        };
    });
    
})();
